import com.devcamp.Circle;
import com.devcamp.Rectangle;
import com.devcamp.Shape;
import com.devcamp.Square;

public class App {
    public static void main(String[] args) throws Exception {
        
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);
        System.out.println(shape1);
        System.out.println(shape2);

        // Circle circle1 = new Circle();
        // Circle circle2 = new Circle(2.0);
        // Circle circle3 = new Circle(3.0, "green", true);
        // System.out.println("Circle 1[Area= " + circle1.getArea() + ",  Perimeter= " + circle1.getPerimeter() + "]");
        // System.out.println("Circle 2[Area= " + circle2.getArea() + ",  Perimeter= " + circle2.getPerimeter() + "]");
        // System.out.println("Circle 3[Area= " + circle3.getArea() + ",  Perimeter= " + circle3.getPerimeter() + "]");
        // System.out.println(circle1);
        // System.out.println(circle2);
        // System.out.println(circle3);

        // Rectangle rectangle1 = new Rectangle();
        // Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        // Rectangle rectangle3 = new Rectangle(2.0, 1.5, "green", true);
        // System.out.println("rectangle 1[Area= " + rectangle1.getArea() + ",  Perimeter= " + rectangle1.getPerimeter() + "]");
        // System.out.println("rectangle 2[Area= " + rectangle2.getArea() + ",  Perimeter= " + rectangle2.getPerimeter() + "]");
        // System.out.println("rectangle 3[Area= " + rectangle3.getArea() + ",  Perimeter= " + rectangle3.getPerimeter() + "]");
        // System.out.println(rectangle1);
        // System.out.println(rectangle2);
        // System.out.println(rectangle3);

        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square(2.0, "green", true);
        System.err.println(square1);
        System.err.println(square2);
        System.err.println(square3);
        System.out.println("square 1[Area= " + square1.getArea() + ",  Perimeter= " + square1.getPerimeter() + "]");
        System.out.println("square 2[Area= " + square2.getArea() + ",  Perimeter= " + square2.getPerimeter() + "]");
        System.out.println("square 3[Area= " + square3.getArea() + ",  Perimeter= " + square3.getPerimeter() + "]");

    }
}
